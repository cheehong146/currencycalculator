//
//  ChartHelper.swift
//  CurrencyConverterApp
//
//  Created by Lan Chee Hong on 31/01/2021.
//

import Foundation
import Charts

class ChartXAxisFormatter: IAxisValueFormatter {
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let date = Date(timeIntervalSince1970: value)
        return DateUtil.getStringFrom(date, .ddMMyyyy)
    }
}
