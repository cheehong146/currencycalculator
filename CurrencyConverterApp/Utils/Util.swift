//
//  Util.swift
//  CurrencyConverterApp
//
//  Created by Lan Chee Hong on 30/01/2021.
//

import Foundation
import SystemConfiguration
import UIKit

enum Currency: String, CaseIterable {
    case cad = "CAD"
    case hkd = "HKD"
    case isk = "ISK"
    case php = "PHP"
    case dkk = "DKK"
    case huf = "HUF"
    case czk = "CZK"
    case aud = "AUD"
    case ron = "RON"
    case sek = "SEK"
    case idr = "IDR"
    case inr = "INR"
    case brl = "BRL"
    case rub = "RUB"
    case hrk = "HRK"
    case jpy = "JPY"
    case thb = "THB"
    case chf = "CHF"
    case sgd = "SGD"
    case pln = "PLN"
    case bgn = "BGN"
    case turkishLira = "TRY" //renamed due to reserved 'try'
    case cny = "CNY"
    case nok = "NOK"
    case nzd = "NZD"
    case zar = "ZAR"
    case usd = "USD"
    case mxn = "MXN"
    case ils = "ILS"
    case gbp = "GBP"
    case krw = "KRW"
    case myr = "MYR"
    case eur = "EUR"
}


public class Reachability {
    class func isConnectedToNetwork() -> Bool {

        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)

        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }

        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }

        /* Only Working for WIFI
        let isReachable = flags == .reachable
        let needsConnection = flags == .connectionRequired

        return isReachable && !needsConnection
        */

        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)

        return ret

    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

class Util {
    
    public static func getRateFrom(latestData: LatestDataType, _ currency: Currency) -> Rate? {
        for rate in latestData.rates {
            if rate.symbol == currency.rawValue {
                return rate
            }
        }
        return nil
    }
}
