//
//  DateUtil.swift
//  CurrencyConverterApp
//
//  Created by Lan Chee Hong on 31/01/2021.
//

import Foundation
enum DateFormat: String {
    case yyyyMMdd = "yyyy-MM-dd"
    case ddMMyyyy = "dd/MM/yyyy"
}

class DateUtil {
    
    private static let kDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = TimeZone.ReferenceType.local
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
    
    
    static func getDateFrom(_ date: String, _ dateFormat: DateFormat) -> Date {
        kDateFormatter.dateFormat = dateFormat.rawValue
        return kDateFormatter.date(from: date) ?? Date()
    }
    
    static func getStringFrom(_ date: Date, _ dateFormat: DateFormat) -> String {
        kDateFormatter.dateFormat = dateFormat.rawValue
        return kDateFormatter.string(from: date)
    }
    
    /// return a date of curDate + days; for numofDays use negative num for past date and positive num for future date
    static func getDateFrom(curDate: Date, numofDays: Int) -> Date {
        var dayComponent    = DateComponents()
        dayComponent.day    = numofDays
        let theCalendar     = Calendar.current
        let nextDate        = theCalendar.date(byAdding: dayComponent, to: curDate)
        return nextDate!
    }
}
