//
//  DialogHelper.swift
//  CurrencyConverterApp
//
//  Created by Lan Chee Hong on 31/01/2021.
//

import Foundation
import SVProgressHUD

class DialogHelper {
    
    private init() {}
    
    public static func initialize() {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setForegroundColor(UIColor.cyan)
    }
    
    public static func showProgress() {
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
    }
    
    public static func hideProgress() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
}
