//
//  ViewController.swift
//  CurrencyConverterApp
//
//  Created by Lan Chee Hong on 30/01/2021.
//

import UIKit
import RealmSwift
import Charts

enum DropDownSelector: Int {
    case fromCurrency = 0
    case toCurrency = 1
}

enum SegmentButtonIndex: Int {
    case rate = 0
    case chart = 1
}

enum BottomContainerViewType: Int{
    case exchangeRate = 0
    case chart = 1
}

class ViewController: UIViewController {
    
    // UI Design copy from 'https://collectui.com/designers/vishikh/currency-converter'
    
    @IBOutlet weak var topBarContainerView: UIView!
    @IBOutlet weak var topContainerView: UIView!
    @IBOutlet weak var bottomContainerView: UIView!
    
    /// Top Bar Container Views
    @IBOutlet weak var fromCurrencyLabel: UILabel!
    
    /// Top Container Views
    @IBOutlet weak var inputValueTextField: UITextField!
    
    /// Bottom Container Views
    @IBOutlet weak var toCurrencyLabel: UILabel!
    @IBOutlet weak var segmentButton: UISegmentedControl!
    @IBOutlet weak var toCurrencyValueLabel: UILabel!
    @IBOutlet weak var lineChartView: LineChartView!
    
    /// Misc Views
    @IBOutlet weak var pickerViewContainer: UIStackView!
    @IBOutlet weak var pickerViewToolBar: UIToolbar!
    @IBOutlet weak var currencyPickerView: UIPickerView!
    
    private var fromCurrency: Currency = .usd
    private let defaultToCurrency: Currency = .gbp
    private lazy var toCurrency: Currency = defaultToCurrency
    
    private var currencyPickerData: [Currency] = []
    private let numOfPrevDays = 7 // chart data look back days amt
    
    private var latestData: LatestDataType?
    private var historicData: HistoricDataType?
    
    // MARK: - Init Load
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        fetchBaseCurrencyData(fromCurrency)
        hideKeyboardWhenTappedAround()
    }
    
    private func setupView() {
        inputValueTextField.placeholder = "Enter Amount of \(fromCurrency.rawValue)"
        inputValueTextField.delegate = self
        
        fromCurrencyLabel.text = fromCurrency.rawValue
        toCurrencyValueLabel.text = "-"
        
        updateBottomView(.exchangeRate)
        
        //picker view
        pickerViewContainer.isHidden = true
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(didSelectPickerView(sender:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        pickerViewToolBar.setItems([spaceButton, doneButton], animated: false)
        currencyPickerView.dataSource = self
        currencyPickerView.delegate = self
        pickerViewContainer.isHidden = true
        pickerViewContainer.alpha = 0
        
        // charts
        lineChartView.drawGridBackgroundEnabled = false
        lineChartView.rightAxis.enabled = false
        lineChartView.xAxis.centerAxisLabelsEnabled = false
    }
    
    // MARK: - API Calls
    private func fetchBaseCurrencyData(_ currency: Currency) {
        DialogHelper.showProgress()
        if Reachability.isConnectedToNetwork() {
            APICall.getLatest(currency) { (data, error) in
                DialogHelper.hideProgress()
                
                if let error = error {
                    // TODO ui showing user something went wrong with api
                    print(error)
                    return
                }
                
                if let data = data {
                    self.latestData = data
                    let realm = try! Realm()
                    try! realm.write({
                        realm.add(data)
                    })
                    
                    self.updateView(data)
                }
            }
        } else {
            DialogHelper.hideProgress()
            // get last saved data from realm
            let realm = try! Realm()
            self.latestData = realm.objects(LatestDataType.self).last
            
            guard let latestData = self.latestData else { return }
            self.updateView(latestData)
        }
    }
    
    private func fetchChartData(_ startDate: String, _ endDate: String, _ fromCurrency: Currency, _ toCurrency: Currency) {
        DialogHelper.showProgress()
        if Reachability.isConnectedToNetwork() {
            APICall.getHistoryRate(startDate: startDate, endDate: endDate, fromCurrency, toCurrency) { (data, error) in
                DialogHelper.hideProgress()
                
                if let error = error {
                    // TODO ui showing user something went wrong with api
                    print(error)
                    return
                }
                
                if let data = data {
                    self.historicData = data
                    let realm = try! Realm()
                    try! realm.write({
                        realm.add(data)
                    })
                    
                    self.updateChartView(data)
                }
            }
        } else {
            DialogHelper.hideProgress()
            // get last saved data from realm
            let realm = try! Realm()
            self.historicData = realm.objects(HistoricDataType.self).last
            
            guard let historicData = self.historicData else { return }
            self.updateChartView(historicData)
        }
    }
    
    // MARK: - Views handling
    private func updateBottomView(_ type: BottomContainerViewType) {
        UIView.animate(withDuration: 0.3) {
            self.toCurrencyValueLabel.alpha = (type == .exchangeRate) ? 1.0 : 0.0
            self.lineChartView.alpha = (type == .chart) ? 1.0 : 0.0
        } completion: { _ in
            self.toCurrencyValueLabel.isHidden = type != .exchangeRate
            self.lineChartView.isHidden = type != .chart
        }
        
    }
    
    private func updateView(_ data: LatestDataType) {
        // set the first received rate as default conversion
        guard let fromCurrency = Currency(rawValue: data.base ?? "") else { return }
        self.fromCurrency = fromCurrency
        fromCurrencyLabel.text = fromCurrency.rawValue
        inputValueTextField.text = ""
        inputValueTextField.placeholder = "Enter Amount of \(fromCurrency.rawValue)"
        
        self.toCurrency = Currency(rawValue: Util.getRateFrom(latestData: data, defaultToCurrency)?.symbol ?? "") ?? defaultToCurrency
        toCurrencyLabel.text = toCurrency.rawValue
        updateToCurrencyLabel()
    }
    
    private func updateChartView(_ data: HistoricDataType) {
        updateBottomView(.chart)
        
        let fromCurrencyVal: Double = Double(inputValueTextField.text ?? "1.0") ?? 1.0
        
        var lineChartEntry: [ChartDataEntry] = []
        
        var dateRange: [String] = []
        var rateRange: [Float] = []
        
        // sort data by ascending date due to charts needs to take in ordered data
        let sortedHistoricRates = data.rates.sorted { (cur, next) -> Bool in
            guard let curDate = cur.date else { return false }
            guard let nextDate = next.date else { return  false }
            return curDate.compare(nextDate) == .orderedAscending
        }
        
        for historicRate in sortedHistoricRates {
            dateRange.append(historicRate.date ?? "")
            
            for rate in historicRate.rate {
                if rate.symbol == toCurrency.rawValue {
                    rateRange.append(rate.value)
                }
            }
        }
        
        if dateRange.count != rateRange.count { return }
        
        for i in 0...(dateRange.count - 1) {
            // x axis data
            let dateData = DateUtil.getDateFrom(dateRange[i], .yyyyMMdd)
            let timeInterval = dateData.timeIntervalSince1970
            // y axis data
            let finalExchangedVal = Double(rateRange[i]) * fromCurrencyVal
            lineChartEntry.append(ChartDataEntry(x: timeInterval, y: finalExchangedVal))
        }
        
        let line = LineChartDataSet(entries: lineChartEntry, label: "Rates")
        line.colors = [UIColor.cyan]
        line.drawFilledEnabled = true
        line.drawCirclesEnabled = false
        line.axisDependency = .left
        
        let data = LineChartData()
        data.setDrawValues(false)
        data.addDataSet(line)
        
        lineChartView.data = data
        
        // chart formatting
        lineChartView.xAxis.valueFormatter = ChartXAxisFormatter()
        lineChartView.chartDescription?.text = "Exchange rate of \(String(format: "%.2f", arguments: [fromCurrencyVal])) \(fromCurrency.rawValue) to \(toCurrency.rawValue)"
        lineChartView.xAxis.setLabelCount(dateRange.count, force: true)
    }
    
    private func updateToCurrencyLabel() {
        guard let latestData = latestData else { return }
        guard let fromCurrencyVal = Float(inputValueTextField.text ?? "-1.0") else {
            toCurrencyValueLabel.text = "-"
            return
        }
        
        let toRate = Util.getRateFrom(latestData: latestData, toCurrency)?.value ?? 1.0
        let toCurrencyValAmount = fromCurrencyVal * toRate
        toCurrencyValueLabel.text = "\(String.init(format: "%.2f", arguments: [toCurrencyValAmount]))"
    }
    
    private func showCurrencyPickerView(isFromCurrency: Bool, data: [Currency]) {
        currencyPickerData = data
        currencyPickerView.tag = isFromCurrency ? DropDownSelector.fromCurrency.rawValue : DropDownSelector.toCurrency.rawValue
        currencyPickerView.reloadAllComponents()
        
        var toScrollToIndex = 0
        let currencyToSearch = isFromCurrency ? fromCurrency : Currency(rawValue: toCurrencyLabel.text ?? "")
        for i in 0...(data.count - 1) {
            if data[i] == currencyToSearch {
                toScrollToIndex = i
            }
        }
        
        currencyPickerView.selectRow(toScrollToIndex, inComponent: 0, animated: false)
        
        self.pickerViewContainer.isHidden = false
        UIView.animate(withDuration: 0.3) {
            self.pickerViewContainer.alpha = 1
        }
    }
    
    private func dismissCurrencyPickerView() {
        UIView.animate(withDuration: 0.3) {
            self.pickerViewContainer.alpha = 0
        } completion: { _ in
            self.pickerViewContainer.isHidden = true
        }
        
    }
    
    // MARK: - Actions
    @objc private func didSelectPickerView(sender: UIBarButtonItem) {
        dismissCurrencyPickerView()
        
        let selectedCurrency = currencyPickerData[currencyPickerView.selectedRow(inComponent: 0)]
        switch currencyPickerView.tag {
        case DropDownSelector.fromCurrency.rawValue:
            if selectedCurrency != self.fromCurrency {
                self.fromCurrency = selectedCurrency
                fromCurrencyLabel.text = selectedCurrency.rawValue
                fetchBaseCurrencyData(selectedCurrency)
            }
        case DropDownSelector.toCurrency.rawValue:
            if selectedCurrency.rawValue == toCurrency.rawValue { return }
            self.toCurrency = selectedCurrency
            
            switch segmentButton.selectedSegmentIndex {
            case SegmentButtonIndex.rate.rawValue:
                guard let latestData = latestData else { return }
                self.toCurrency = Currency(rawValue: Util.getRateFrom(latestData: latestData, selectedCurrency)?.symbol ?? "") ?? defaultToCurrency
                updateToCurrencyLabel()
                
            case SegmentButtonIndex.chart.rawValue:
                let endDateStr = DateUtil.getStringFrom(Date(), .yyyyMMdd)
                let startDate = DateUtil.getDateFrom(curDate: Date(), numofDays: -numOfPrevDays)
                let startDateStr = DateUtil.getStringFrom(startDate, .yyyyMMdd)
                
                fetchChartData(startDateStr, endDateStr, fromCurrency, selectedCurrency)
            default:
                return
            }
            
            self.toCurrencyLabel.text = selectedCurrency.rawValue
            
        default:
            return
        }
    }
    
    @IBAction func fromCurrencyButtonClick(_ sender: Any) {
        showCurrencyPickerView(isFromCurrency: true, data: Currency.allCases)
    }
    
    @IBAction func toCurrencyButtonClick(_ sender: Any) {
        guard let rate = latestData?.rates else { return }
        var currencyArr: [Currency] = []
        rate.forEach { (rate) in
            if let currency = Currency(rawValue: rate.symbol!) {
                currencyArr.append(currency)
            }
        }
        showCurrencyPickerView(isFromCurrency: false, data: currencyArr)
    }
    
    @IBAction func segmentButtonOnChange(_ sender: Any) {
        guard let segmentButton = sender as? UISegmentedControl else { return }
        switch segmentButton.selectedSegmentIndex {
        
        case SegmentButtonIndex.rate.rawValue:
            updateBottomView(.exchangeRate)
            
        case SegmentButtonIndex.chart.rawValue:
            updateBottomView(.chart)
            
            let endDateStr = DateUtil.getStringFrom(Date(), .yyyyMMdd)
            let startDate = DateUtil.getDateFrom(curDate: Date(), numofDays: -numOfPrevDays)
            let startDateStr = DateUtil.getStringFrom(startDate, .yyyyMMdd)
            
            fetchChartData(startDateStr, endDateStr, fromCurrency, toCurrency)
        default:
            return
        }
    }
}

// MARK: - UITextFieldDelegate
extension ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string.canBeConverted(to: .ascii) || string == "" {
            if Int(string) != nil || string == "." || string == "" {
                if string == "." && textField.text?.contains(".") ?? true  {
                    return false
                }
                
                if string == "" {
                    textField.text?.removeLast()
                } else {
                    textField.text = "\(textField.text ?? "")\(string)"
                }
                
                if self.segmentButton.selectedSegmentIndex == SegmentButtonIndex.rate.rawValue {
                    updateToCurrencyLabel()
                }
                
                return false
            }
        }
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if segmentButton.selectedSegmentIndex == SegmentButtonIndex.chart.rawValue {
            let endDateStr = DateUtil.getStringFrom(Date(), .yyyyMMdd)
            let startDate = DateUtil.getDateFrom(curDate: Date(), numofDays: -numOfPrevDays)
            let startDateStr = DateUtil.getStringFrom(startDate, .yyyyMMdd)
            
            fetchChartData(startDateStr, endDateStr, fromCurrency, toCurrency)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}

// MARK: - UIPickerViewDelegate, UIPickerViewDataSource
extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.currencyPickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.currencyPickerData[row].rawValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        return
    }
}
