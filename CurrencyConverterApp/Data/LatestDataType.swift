//
//  File.swift
//  CurrencyConverterApp
//
//  Created by Lan Chee Hong on 30/01/2021.
//

import Foundation
import RealmSwift

class LatestDataType: Object, Decodable {
    @objc dynamic var date: String?
    @objc dynamic var base: String?
    var rates = List<Rate>()
    
    enum CodingKeys: String, CodingKey{
        case date
        case base
        case rates
    }
    
    public required convenience init(from decoder: Decoder) throws {
        self.init()
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.date = try values.decode(String.self, forKey: .date)
        self.base = try values.decode(String.self, forKey: .base)
        let ratesDict = try values.decode([String: Float].self, forKey: .rates)
        
        for rate in ratesDict {
            let rateObj = Rate()
            rateObj.symbol = rate.key
            rateObj.value = rate.value
            self.rates.append(rateObj)
        }
    }
    
}

class Rate: Object {
    @objc dynamic var symbol: String?
    @objc dynamic var value: Float = 0.0
}
