//
//  HistoricDataType.swift
//  CurrencyConverterApp
//
//  Created by Lan Chee Hong on 31/01/2021.
//

import Foundation
import RealmSwift

class HistoricDataType: Object, Decodable {
    @objc dynamic var startDate: String?
    @objc dynamic var endDate: String?
    @objc dynamic var base: String?
    var rates = List<HistoricRate>()
    
    enum CodingKeys: String, CodingKey{
        case startDate = "start_at"
        case endDate = "end_at"
        case base
        case rates
    }
    
    public required convenience init(from decoder: Decoder) throws {
        self.init()
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.startDate = try values.decode(String.self, forKey: .startDate)
        self.endDate = try values.decode(String.self, forKey: .endDate)
        self.base = try values.decode(String.self, forKey: .base)
        let ratesDict = try values.decode([String: [String: Float]].self, forKey: .rates)
        
        for date in ratesDict {
            let historicObj = HistoricRate()
            historicObj.date = date.key
            
            for rate in date.value {
                let rateObj = Rate()
                rateObj.symbol = rate.key
                rateObj.value = rate.value
                historicObj.rate.append(rateObj)
            }
            self.rates.append(historicObj)
        }
    }
    
}

class HistoricRate: Object {
    @objc dynamic var date: String?
    var rate = List<Rate>()
}

