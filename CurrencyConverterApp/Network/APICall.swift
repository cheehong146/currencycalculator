//
//  APICall.swift
//  CurrencyConverterApp
//
//  Created by Lan Chee Hong on 30/01/2021.
//

import Foundation
import Alamofire
import RealmSwift

class APICall {
    
    public static func getLatest(_ baseCurrency: Currency? = .eur, completion: @escaping (LatestDataType?, Error?) -> Void) {
        let request = APIRequest(
            apiResource: .latest,
            method: .get,
            subpath: "",
            params: ["base": baseCurrency?.rawValue ?? ""])
        APIClient.shared.performAPICall(request).responseJSON { (response) in
            if let error = response.error {
                completion(nil, error)
                return
            }
            
            if let data = response.data {
                do {
                    print(data.prettyJson ?? "")
                    let dataObj = try JSONDecoder().decode(LatestDataType.self, from: data)
                    completion(dataObj, nil)
//                    self.storeData(jsonData)
                } catch let error as NSError {
                    print(error)
                }
            }
        }
    }
    
    public static func getCurrencyRate(from: Currency, to: Currency, completion: @escaping (LatestDataType?, Error?) -> Void) {
        let request = APIRequest(
            apiResource: .latest,
            method: .get,
            subpath: "",
            params: ["symbols": "\(from.rawValue),\(to.rawValue)"])
        APIClient.shared.performAPICall(request).responseJSON { (response) in
            if let error = response.error {
                completion(nil, error)
                return
            }
            
            if let data = response.data {
                do {
                    print(data.prettyJson ?? "")
                    let dataObj = try JSONDecoder().decode(LatestDataType.self, from: data)
                    completion(dataObj, nil)
//                    self.storeData(jsonData)
                } catch let error as NSError {
                    print(error)
                }
            }
        }
    }
    
    /// date formate must be in yyyy-MM-dd
    public static func getHistoryRate(startDate: String, endDate: String, _ fromCurrency: Currency, _ toCurrency: Currency ,completion: @escaping (HistoricDataType?, Error?) -> Void) {
        let param: [String: Any] = [
            "start_at": startDate,
            "end_at": endDate,
            "base": "\(fromCurrency.rawValue)",
            "symbols": "\(toCurrency.rawValue)"
        ]
        let request = APIRequest(apiResource: .history, method: .get, subpath: "", params: param)
        APIClient.shared.performAPICall(request).responseJSON { (response) in
            if let error = response.error {
                completion(nil, error)
                return
            }
            
            if let data = response.data {
                do {
                    print(data.prettyJson ?? "")
                    let dataObj = try JSONDecoder().decode(HistoricDataType.self, from: data)
                    completion(dataObj, nil)
//                    self.storeData(jsonData)
                } catch let error as NSError {
                    print(error)
                }
            }
        }
    }
    
    private func storeData(_ data: LatestDataType) {
        let realm = try! Realm()
//        try! realm.write {
//            realm.add(data)
//        }
    }
}
