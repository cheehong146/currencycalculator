//
//  APIResouces.swift
//  CurrencyConverterApp
//
//  Created by Lan Chee Hong on 30/01/2021.
//

public enum APIMethod: String {
    case get = "GET"
    case post = "POST"
}


public enum APIResources {
    private static let baseUrl = "https://api.exchangeratesapi.io/"
    
    case base
    case latest
    case history
    
    public var path: String {
        switch self {
        case .base:
            return ""
        case .latest:
            return "latest"
        case .history:
            return "history"
        }
    }
    
    public var url: String {
        return "\(APIResources.baseUrl)\(self.path)"
    }
}
