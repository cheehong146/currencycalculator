//
//  APIRequest.swift
//  CurrencyConverterApp
//
//  Created by Lan Chee Hong on 30/01/2021.
//

class APIRequest {
    var apiResource: APIResources
    var method: APIMethod
    var subpath: String = ""
    var params: [String: Any] = [:]
    
    init(apiResource: APIResources, method: APIMethod, subpath: String, params: [String: Any]) {
        self.apiResource = apiResource
        self.method = method
        self.subpath = subpath
        self.params = params
    }
}
