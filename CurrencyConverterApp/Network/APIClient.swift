//
//  APIClient.swift
//  CurrencyConverterApp
//
//  Created by Lan Chee Hong on 30/01/2021.
//

import Alamofire

class APIClient {
    public static var shared = APIClient()
    
    func performAPICall(_ request: APIRequest) -> DataRequest {
        // TODO update here if there's any POST API call to add another layer
        print("===== Request =====")
        print(Date())
        let fullUrl = request.apiResource.url + request.subpath
        let param = request.params as Parameters
        print(fullUrl)
        print(param)
        
        let alamofireRequest =  AF.request(fullUrl, parameters: param)
        return alamofireRequest
    }
    
}
