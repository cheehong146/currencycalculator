Improvement that could be made:
- better UI
- custom error dialog box show on api error
- stop calling api for chart data everytime user switch between exchange rate and chart (implement cache and only call api when params changed)
- changing to currency while on 'Chart' screen then switching to 'Rate' will diplay old data (need to call latest api and cache it and only call again when the params changes when compared to cache)

Things to take into consideration:
- 'https://api.exchangeratesapi.io/history?start_at=2018-01-01&end_at=2018-09-01&base=USD' api only returns mon-fri data hence the 7 days rule if implemented would be 7 working days instead. Settled on 5 working days instead.
